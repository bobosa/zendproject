SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `ZendProject` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `ZendProject` ;

-- -----------------------------------------------------
-- Table `ZendProject`.`User`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ZendProject`.`User` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `firstName` VARCHAR(45) NOT NULL,
  `middleName` VARCHAR(45) NULL,
  `lastName` VARCHAR(45) NOT NULL,
  `mobile` VARCHAR(30) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `country` VARCHAR(45) NOT NULL,
  `birthday` DATE NOT NULL,
  `image` VARCHAR(45) NOT NULL,
  `gender` ENUM('male','female') NULL,
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
  `userName` VARCHAR(50) NULL,
  `password` VARCHAR(32) NULL,
>>>>>>> 7e8cff0aeec48154b42b81094dbf8e6df15cfdb0
>>>>>>> 1bfaf0fe3b63bda0222d37ac45ff979e64ed8bc9
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `mobile_UNIQUE` (`mobile` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB
COMMENT = 'Trannie table that want to be trained on specific courses';


-- -----------------------------------------------------
-- Table `ZendProject`.`Tag`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ZendProject`.`Tag` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `describtion` TEXT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ZendProject`.`Course`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ZendProject`.`Course` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `describtion` VARCHAR(45) NULL,
  `path` VARCHAR(45) NULL,
<<<<<<< HEAD
  `TeachedBy` VARCHAR(45) NULL,
  `publishDate` VARCHAR(45) NULL,
<<<<<<< HEAD
=======
=======
  `TeachedBy` INT NULL,
  `publishDate` DATETIME NULL,
>>>>>>> 7e8cff0aeec48154b42b81094dbf8e6df15cfdb0
>>>>>>> 1bfaf0fe3b63bda0222d37ac45ff979e64ed8bc9
  `status` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_Course_1_idx` (`TeachedBy` ASC),
  CONSTRAINT `fk_Course_1`
    FOREIGN KEY (`TeachedBy`)
    REFERENCES `ZendProject`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ZendProject`.`Admin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ZendProject`.`Admin` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(32) NOT NULL,
  `img` VARCHAR(45) NULL,
  `lastLogIn` DATE NULL,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ZendProject`.`Post`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ZendProject`.`Post` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `textArea` TEXT NOT NULL,
  `date` DATETIME NOT NULL,
  `UserId` INT NOT NULL,
  `CourseId` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_Post_Course1_idx` (`CourseId` ASC),
  INDEX `fk_Post_User1_idx` (`UserId` ASC),
  CONSTRAINT `fk_Post_Course1`
    FOREIGN KEY (`CourseId`)
    REFERENCES `ZendProject`.`Course` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Post_User1`
    FOREIGN KEY (`UserId`)
    REFERENCES `ZendProject`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ZendProject`.`Comment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ZendProject`.`Comment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `textArea` TEXT NOT NULL,
  `date` DATETIME NOT NULL,
  `UserId` INT NOT NULL,
  `PostId` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_Comment_Post_idx` (`PostId` ASC),
  INDEX `fk_Comment_User1_idx` (`UserId` ASC),
  CONSTRAINT `fk_Comment_Post`
    FOREIGN KEY (`PostId`)
    REFERENCES `ZendProject`.`Post` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Comment_User1`
    FOREIGN KEY (`UserId`)
    REFERENCES `ZendProject`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ZendProject`.`Like`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ZendProject`.`Like` (
  `PostId` INT NOT NULL,
  `UserId` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`PostId`, `UserId`),
  UNIQUE INDEX `UserId_UNIQUE` (`UserId` ASC),
  UNIQUE INDEX `PostId_UNIQUE` (`PostId` ASC),
  CONSTRAINT `fk_Like_Post1`
    FOREIGN KEY (`PostId`)
    REFERENCES `ZendProject`.`Post` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ZendProject`.`Type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ZendProject`.`Type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ZendProject`.`Material`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ZendProject`.`Material` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `isLocked` ENUM('0','1') NOT NULL,
  `isShown` ENUM('0','1') NOT NULL,
  `numberOfDownloads` INT NOT NULL,
  `parent` INT NULL,
  `TypeId` INT NOT NULL,
  `CourseId` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_Material_Type1_idx` (`TypeId` ASC),
  INDEX `fk_Material_Course1_idx` (`CourseId` ASC),
  INDEX `fk_Material_Parent_idx` (`parent` ASC),
  CONSTRAINT `fk_Material_Type1`
    FOREIGN KEY (`TypeId`)
    REFERENCES `ZendProject`.`Type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Material_Course1`
    FOREIGN KEY (`CourseId`)
    REFERENCES `ZendProject`.`Course` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Material_Parent`
    FOREIGN KEY (`parent`)
    REFERENCES `ZendProject`.`Material` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ZendProject`.`UserPreferedTopics`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ZendProject`.`UserPreferedTopics` (
  `UserId` INT NOT NULL,
  `TagId` INT NOT NULL,
  PRIMARY KEY (`UserId`, `TagId`),
  INDEX `fk_User_has_Tag_Tag1_idx` (`TagId` ASC),
  INDEX `fk_User_has_Tag_User1_idx` (`UserId` ASC),
  UNIQUE INDEX `UserId_UNIQUE` (`UserId` ASC),
  UNIQUE INDEX `TagId_UNIQUE` (`TagId` ASC),
  CONSTRAINT `fk_User_has_Tag_User1`
    FOREIGN KEY (`UserId`)
    REFERENCES `ZendProject`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_has_Tag_Tag1`
    FOREIGN KEY (`TagId`)
    REFERENCES `ZendProject`.`Tag` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ZendProject`.`CourseHasTag`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ZendProject`.`CourseHasTag` (
  `CourseId` INT NOT NULL,
  `TagId` INT NOT NULL,
  PRIMARY KEY (`CourseId`, `TagId`),
  INDEX `fk_Course_has_Tag_Tag1_idx` (`TagId` ASC),
  INDEX `fk_Course_has_Tag_Course1_idx` (`CourseId` ASC),
  UNIQUE INDEX `CourseId_UNIQUE` (`CourseId` ASC),
  UNIQUE INDEX `TagId_UNIQUE` (`TagId` ASC),
  CONSTRAINT `fk_Course_has_Tag_Course1`
    FOREIGN KEY (`CourseId`)
    REFERENCES `ZendProject`.`Course` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Course_has_Tag_Tag1`
    FOREIGN KEY (`TagId`)
    REFERENCES `ZendProject`.`Tag` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
