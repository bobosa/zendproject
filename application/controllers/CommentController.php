<?php

class CommentController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
    }
    
    public function addAction()
    {
       // $form=new Application_Form_Comment();
        if($this->_request->isPost())
        {   
               $auth = Zend_Auth::getInstance();
               $storage = $auth->getStorage()->read();
               $comment_info = $this->_request->getParams();
               $postId=$this->_request->getParam("id");
               $userId=$storage->id;
               $comment_info['UserId']=$userId;
               $comment_info['PostId']=$postId;
               var_dump($comment_info);
               $comment_form = new Application_Form_Comment();
               if($comment_form->isValid($comment_info)){
                  $comment=  $comment_form->getValues();
//                  var_dump($comment);
                  $comment_model = new Application_Model_Comment();
                  $comment_model->addComment($comment_info);
               }
               $post=new Application_Model_Post();
               $selectedPost=$post->getPostById($postId);
               $this->redirect("post/form/id/".$selectedPost[0]['CourseId']);
                       
        }
      //  $this->view->form=$form;
    }
    
    public function editAction() {
        $id = $this->_request->getParam("id"); //always comes from get not post
        $form = new Application_Form_Comment();
        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getParams())) {

                $comment_info = $form->getValues();
                $comment_info['id'] = $id;
                $comment_model = new Application_Model_Comment();
                $comment_model->editComment($comment_info);
            }
        }
        if (!empty($id)) {

            $comm_model = new Application_Model_Comment();
            $com = $comm_model->getCommentId($id);

            $form->populate($com[0]);
//         
        } else {
            $this->redirect("post/form/id/$id");
        }
        $this->view->form = $form;
        $this->render('add');
    }


}

