<?php

class TagController extends Zend_Controller_Action
{
     public function init()
    {
        /* Initialize action controller here */
    }

   public function indexAction()
    {
        // action body
    }
    
    public function addAction()
    {
      $form=new Application_Form_Tag();
      $auth = Zend_Auth::getInstance();
      $storage = $auth->getStorage()->read();
      if($this->_request->isPost()){
           if($form->isValid($this->_request->getParams())){
              $tag_info = $form->getValues();
//             
             $model = new Application_Model_Tags();
//              
            $reault=$model->addTag($tag_info);
            $this->redirect("user");
              
           }
       }
      if($storage->id!="")
      {
          $this->view->form = $form;
      }
      else 
      {
          $this->redirect("user/login");
      }
    }
    public function listAction()
    {
        $Tag_model = new Application_Model_Tags();
        $auth = Zend_Auth::getInstance();
        $storage = $auth->getStorage()->read();
        if($storage->id!="")
        {
              $this->view->Tags = $Tag_model->listTags();
        }
        else 
        {
            $this->redirect("user/login");
        }
      
    }
     public function deleteAction()
    {
        $id = $this->_request->getParam("id");
        $Tag_model = new Application_Model_Tags();
        $auth = Zend_Auth::getInstance();
        $storage = $auth->getStorage()->read();
        if(!empty($id)&&$storage->type=='admin')
        {
            
            $Tag_model->deleteTag($id);
        }
        
            $this->redirect("Tag/list");
    }
    public function editAction()
    {
        $id = $this->_request->getParam("id");
        $form  = new Application_Form_Tag();
        $auth = Zend_Auth::getInstance();
        $storage = $auth->getStorage()->read();
        if (!empty($id)&&$storage->type=='admin') {
                $tag_model = new Application_Model_Tags();
                $tag = $tag_model->getTagById($id);
               // var_dump($tag);
                $form->populate($tag[0]);
            } else {
                $this->redirect("tag/list");
            }
        if($this->_request->isPost()){
           if($form->isValid($this->_request->getParams())){
               $tag_info = $form->getValues();
               $tag_model = new Application_Model_Tags();
               $tag_info['id']=$id;
               $tag_model->editTag($tag_info);
              // var_dump($tag_info);
                       
           }
        }
        $this->view->form = $form;
	$this->render('add');
    }
}

