<?php

class UserController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        
       $auth = Zend_Auth::getInstance()->getStorage()->read();
       
       $user = new Application_Model_User();
       if(!empty($auth->id))
       {
           $sr = $user->getUserTeachedByCourses($auth->id);
       }
       else {
           $this->redirect("user/login");
        }
        $this->view->Courses=$sr;
    }

    
    
    public function addAction() 
    {
        $form = new Application_Form_User();
        $user_model = new Application_Model_User();
        $preferedtags_model = new Application_Model_UserPreferedTopics();
        $request = $this->getRequest();
        
         
        if ($this->getRequest()->isPost())
        {
            if ($form->isValid($request->getPost())) 
            {
                $user_info = $form->getValues();
                $userid = $user_model->addUser($user_info);
                
                
                
                $preferedtags_info = $form->getValue("preferedtags");
                $preferedtags_model->addUserPreferedTopics($userid, $preferedtags_info);
                return $this->_helper->redirector('index');
            }
       }

        $this->view->form = $form;
    }
    
    public function loginAction()
    {
        $form = new Application_Form_Login();
        
        $this->view->form = $form;

        if($this->_request->isPost())
        {
            if($form->isValid($this->_request->getParams()))
            {
                $username = $this->_request->getParam("username");
                $password = $this->_request->getParam("password");
                
                
                /*
                 * get connection
                 */
                $db = Zend_Db_Table::getDefaultAdapter();
                
                /*
                 * object from table student has data all data of 2 columns. 
                 */
                $authAdapter = new Zend_Auth_Adapter_DbTable($db,'User',"userName", 'password');
                $authAdapter2 = new Zend_Auth_Adapter_DbTable($db,'User',"email", 'password');
                $authAdapter3 = new Zend_Auth_Adapter_DbTable($db,'User',"mobile", 'password');

                
                $authAdapter->setIdentity($username);
                $authAdapter->setCredential(md5($password));
                
                $authAdapter2->setIdentity($username);
                $authAdapter2->setCredential(md5($password));
                
                $authAdapter3->setIdentity($username);
                $authAdapter3->setCredential(md5($password));
                
                $result = $authAdapter->authenticate();
                $result2 = $authAdapter2->authenticate();
                $result3 = $authAdapter3->authenticate();
                
                if ($result->isValid() || $result2->isValid() || $result3->isValid() )
                {
                    
                    $auth = Zend_Auth::getInstance();
                    $storage = $auth->getStorage();
                    $userInfo=array();
                    if($result->isValid())
                       $userInfo=$authAdapter->getResultRowObject(array('id', 'userName' , 'image'));
                   
                    if($result2->isValid())
                        $userInfo=$authAdapter2->getResultRowObject(array('id', 'userName' , 'image'));

                    if($result3->isValid())
                       $userInfo=$authAdapter3->getResultRowObject(array('id', 'userName' , 'image'));
                    
                   $userInfo->type='user';
                   $storage->write($userInfo);
                    echo 'in if';
                    var_dump($storage); 
                    return $this->_helper->redirector('index');
                }
                else
                { 
                  $this->redirect("user/login");
//                    echo 'in else';
//                    echo  $result;
//                    echo '<br/>';
//                    echo  $result2;
//                    echo '<br/>';
//                    echo  $result3;

                }
                
            }
        }
    }
    
    public function editAction()
    {
        $form = new Application_Form_User();
        
        $form->removeElement('password');
        $form->removeElement('confirmpassword');
        $form->removeElement('preferedtags');
        $form->removeElement('username');
        $form->removeElement('email');
        $form->removeElement('mobile');
                
        
        $form->firstname->setRequired(FALSE);
        $form->lastname->setRequired(FALSE);
        $form->middlename->setRequired(FALSE);
        $form->country->setRequired(FALSE);
        $form->image->setRequired(FALSE);
        $form->birthday->setRequired(FALSE);
        $form->gender->setRequired(FALSE);
        $auth = Zend_Auth::getInstance();
        $storage = $auth->getStorage()->read();
        $id =$storage->id;
        
        if(!empty($id))
        {
            $user_model = new Application_Model_User();
            
            $userdata = $user_model->getUserById($id);
            $request = $this->getRequest();
            $filter = new Zend_Filter_StringToLower();
            $data=array();
            
            foreach ($userdata[0] as $selectOption => $value) 
            {
                if( $selectOption=="firstName" || $selectOption=="middleName" || $selectOption=="lastName" || $selectOption=="gender" || $selectOption=="country" ||$selectOption=="birthday")
                    $data[$filter->filter($selectOption)] = $value;
            }
            
            $form->setDefaults($data);
            $form->populate($data);
            
            if($this->_request->isPost())
            {
                if ($form->isValid($request->getPost()))
                {
                    $user_info = $form->getValues();
                $info = array();
                //var_dump($user_info);
                echo '<br/><br/>';
                foreach ($user_info as $selectOption => $value)
                {
                    if($selectOption=="firstname")
                    {
                        $info["firstName"]=$value;
                    }
                    else
                    {
                        if($selectOption=="middlename")
                        {
                            $info["middleName"]=$value;
                        }
                        else
                        {
                            if($selectOption=="lastname")
                            {
                                $info["lastName"]=$value;
                            }
                            else
                            {
                                    $info[$selectOption]=$value;
                            }
                        }
                    }
                }
                }
                
                //var_dump($info);
                $result=$user_model->updateUser($info, $id);
                echo $result;
            }
        }
        
        
        $this->view->form = $form;
        
    }
    
    public function listAction()
    {
        $user_model = new Application_Model_User();
        $this->view->users = $user_model->listUsers();
    }
    public function deleteAction()
    {
        $id = $this->_request->getParam("id");
        if(!empty($id)){
            $user_model = new Application_Model_User();
            $user_model->deleteUser($id);
        }
          
        $this->redirect("user/list");
    }

    public function logoutAction()
    {
        $auth = Zend_Auth::getInstance();
        $auth->clearIdentity();
        $this->_redirect('/user/login');
    }
    
    

}
