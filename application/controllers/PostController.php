<?php

class PostController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {
        // action body
    }


    //*****************************************************************************8
    //To add post

    public function addAction() {
        // action body
        $auth = Zend_Auth::getInstance();
        $storage = $auth->getStorage()->read();
        $form = new Application_Form_Post();

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getParams())) {
                $post_info = $form->getValues();
                $post_model = new Application_Model_Post();

                $date=new Zend_Date();
                $post_info['date']=(string)$date->get('YYYY-MM-dd HH:mm:ss');
                //echo $post_info['date'];
                $post_info['UserId']=$storage->id;
                //$post_info['CourseId']=1;
               // var_dump($post_info);
                $post_model->addPost($post_info);
               // $this->redirect("post/form/id/".$post_info['CourseId']);
            }
        }

        $this->view->post = $form;
    } //addPost

//Endod Asdd post
    //************************************
    //To list all posts

    public function listAction() {
        $auth = Zend_Auth::getInstance();
        $storage = $auth->getStorage()->read();
        if($storage->type==='admin')
        {
            $post_model = new Application_Model_Post();

            $this->view->posts = $post_model->listPost();
        }
        else
        {
            $this->redirect('error/error');
        }
    }//ListPost

    //**************************************
    //To delete specific 
    public function deleteAction() {
        $id = $this->_request->getParam("id");
        $auth = Zend_Auth::getInstance();
        $storage = $auth->getStorage()->read();
        $post_model = new Application_Model_Post();
        $selectedPost=$post_model->getPostById($id);
        if(!empty($id)||$storage->type=='admin'||$selectedPost[0]['UserId']==$storage->id){
            
            $post_model->deletePost($id);
            $this->redirect("post/list");
        }
        else
        {
            $this->redirect("error/error");
        }

        
    } //DeletePost

    //---------EDIT-------------------------
    public function editAction() {
        $id = $this->_request->getParam("id"); //always comes from get not post
        $form = new Application_Form_Post();
        $auth = Zend_Auth::getInstance();
        $storage = $auth->getStorage()->read();
        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getParams())) {
                $post_info = $form->getValues();
                $post_info['id'] = $id;
                $post_model = new Application_Model_Post();
                $post_model->editPost($post_info);                     
            }
        }
        if (!empty($id))
        {

            $pos_model = new Application_Model_Post();
            $pos = $pos_model->getPostById($id);
            if($storage->id==$pos[0]['UserId'])
            {
                $form->populate($pos[0]);
            }
            else
            {
                $this->redirect("error/error");
            }
        } 
        else 
        {
            $this->redirect("post/list/");
        }
        $this->view->form = $form;
        $this->render('add');
    } //Edit Post

    public function formAction() 
    {
        //http://localhost/osalex/public/post/form/id/1
         $id = $this->_request->getParam("id");
         $post_model = new Application_Model_Post();
         $allposts= $post_model->getPostsByCourseId($id);
         $commets=new Application_Model_Comment();
         $user=new Application_Model_User();
         for ($i=0;$i< count($allposts);$i++)
         {
             $allposts[$i]['comments']=$commets->getComments($allposts[$i]['id']);
             for($j=0;$j<count($allposts[$i]['comments']);$j++)
             {
              $allposts[$i]['comments'][$j]['User']=$user->getUserVIInfoById($allposts[$i]['comments'][$j]['UserId']);

             }
             $allposts[$i]['user']=$user->getUserVIInfoById($allposts[$i]['UserId']);
         }
         
         $this->view->posts=$allposts;
         $postform=new Application_Form_Post();
         $postform->id->setValue($id);
         $commentform=new Application_Form_Comment();
         $postform->setAction($this->view->baseUrl().'/post/add');
         $this->view->commentform=$commentform;
         $this->view->postform=$postform;
         
         
    }
  
}
