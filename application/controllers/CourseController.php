<?php

class CourseController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {

        
        $Tag_model = new Application_Model_Tags();
        $this->view->Tags = $Tag_model->listTags();
        
        $course_model = new Application_Model_Course();
        $tag_model = new Application_Form_Tag();
        $TagCourse = new Application_Model_CourseHasTag();
        
        $this->view->Courses = $course_model->listCourses();
        $search=new Application_Form_SearchCourse();
        
        if ($this->getRequest()->isPost())
        {
            if($search->isValid($this->_request->getParams())) 
            {
                $course_info = $search->getValues();
                
               
                
                $this->view->Courses = $course_model->searchCourse("",$course_info['query']);

                
                
                $tagid= $Tag_model->searchTag($course_info['query']);
                //var_dump($tagid);
                $Courses=array();
                for($i=0;$i<count($tagid);$i++)
                {
                    $Courses[$i] = $TagCourse->allCourseWithThatTag($tagid[$i]['id']);
                }

                $this->view->Courses = $Courses[0];
                
            }
        }
        else 
        {
            $this->view->Courses = $course_model->listCourses();
        }
        $this->view->formSearch=$search;
    }
    public function addAction()
    {
        $form= new Application_Form_Course();
        $auth = Zend_Auth::getInstance();
        $storage = $auth->getStorage()->read();
       //  var_dump($storage)
        if ($this->getRequest()->isPost())
        {
            if($form->isValid($this->_request->getParams())) 
            {
                $course_info = $form->getValues();
                $course_model=new Application_Model_Course();
                $course_info['TeachedBy']=$storage->id;
                $course_info['path']=  mkdir($course_info['name'],0777);
                $course_info['path']='../public/'.$course_info['name'];
                
                $result=  $course_model->addCourse($course_info);
                $Course_id=$course_model->getLastCourse();
                $CourseHasTag=new Application_Model_CourseHasTag();
                $CourseHasTag->addCourseTags($Course_id,$course_info['Tags']);
               //var_dump($course_info);
            }
       }
        $this->view->form=$form;
    }


    public function listAction()
    {
        //listCourses()
        
        $auth = Zend_Auth::getInstance();
        $storage = $auth->getStorage()->read();
        if($storage->type==='admin')
        { $course_model = new Application_Model_Course();
          $this->view->Tags = $course_model->listCourses();
        }
        else
        {
            echo "<b> You're Not Authorized</b>";
        }
    }
     public function deleteAction()
    {
        $auth = Zend_Auth::getInstance();
        $storage = $auth->getStorage()->read();
        $id = $this->_request->getParam("id");
        $course=new Application_Model_Course();
        $selectedCourse=$course->getCourseById($id);
        //var_dump($selectedCourse);
     
        if(!empty($id)&&($selectedCourse[0]['TeachedBy']==$storage->id || $storage->type==="admin"))
        {
            $Tag_model = new Application_Model_Course();
            $Tag_model->deleteCourse($id);
            $this->redirect("course/list");
        }
        else
        {
            $this->redirect("error/errors");
        }
            
    }
    public function editAction()
    {
        $id = $this->_request->getParam("id");
        $form  = new Application_Form_Course();
        $auth = Zend_Auth::getInstance();
        $storage = $auth->getStorage()->read();
        if (!empty($id)) 
        {
           $Course_model = new Application_Model_Course();
            $course = $Course_model->getCourseById($id);
               // var_dump($tag);
            if($course[0]['TeachedBy']==$storage->id || $storage->type==="admin")
            {

                $form->populate($course[0]);
            }
            else 
            {
               $this->redirect("error/error"); 
            }
        } 
        else
        {
            $this->redirect("course/list");
        }
         if($this->_request->isPost()){
           if($form->isValid($this->_request->getParams())){
               $tag_info = $form->getValues();
               $tag_model = new Application_Model_Course();
               $tag_info['id']=$id;
               $tags=$tag_info['Tags'];
               unset($tag_info['Tags']);
               $tag_model->editCourse($tag_info,$tags);
              // var_dump($tag_info);
                   $this->redirect("course/list");
     
           }
        }
       
        $this->view->form = $form;
	$this->render('add');
    }
    
    //-----------------khokha
    //---------------View Data of Course according Course Id
    public function viewAction() {
       $id = $this->_request->getParam("id");//From Session
       // $id=1;
                $course_model = new Application_Model_Course();
//                 $this->view->Tags = $course_model->getCourseById($id);
                $data=$course_model->getCourseById($id);
//                var_dump($data);
                $this->view->Tags = $data;
                $instractorid=$data[0]['TeachedBy'];
//                var_dump($instractorid);
                $user_model=new Application_Model_User();
                $instractorata=$user_model->getUserVIInfoById($instractorid);
                //var_dump($instractorata);
                $instname=$instractorata[0]['userName'];
//                var_dump($instname);
                $this->view->Teacher=$instname;
                $tags_model=new Application_Model_CourseHasTag();
                $tags=$tags_model->getTagbyCourseId($id);
//                var_dump("-------------");
//                var_dump(count($tags));
                $myarray=array();
                for($i=0;$i<count($tags);$i++)
                {
//                    var_dump($tags[$i]['TagId']);
                    $tagname_model=new Application_Model_Tags();
                   $namesofTags= $tagname_model->getTagById($tags[$i]['TagId']);
                   $tagName=$namesofTags[0]['name'];
//                    var_dump($tagName);
                  array_push($myarray,$namesofTags);
//                    echo $tagName;
//                    echo "---------";
                   //$myarray[$tags[$i]['TagId']]=$tagName;
                   
                   
                }
                 $this->view->Tagsname=$myarray;
                // $this->view->Tagsname=$myarray;
//                 var_dump($namesofTags[0]['name']);
//                var_dump($tags);
                
//                exit();
//        $this->view->Tags = $data;
    }
//    public function getInstracutorName() {
//        $id=1;
//            
//        
//        
//        
//        
//    }
    
    
}

