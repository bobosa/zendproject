<?php

class MaterialController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {
        // action body
    }

//************************************
    //
    public function addAction() {

       $form = new Application_Form_Material();
       $CourseId=  $this->_request->getParam('CourseId');
       $course=new Application_Model_Course();
       $couesedata=$course->getCourseById($CourseId);
       //var_dump($couesedata);
       echo "/osalex/public/".$couesedata[0]['name'];
      // exit();
       $form->name->setDestination("/var/www/html/osalex/public/".$couesedata[0]['name']);
        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getParams())) {
                $file_info = $form->getValues();
                $x = ($form->getValue("TypeId"));

                $location = $form->name->getFileName();
                $file = file_get_contents($location);

                $fileinfo = $form->name->getFileInfo();
                $filename = $fileinfo;
                $file_model = new Application_Model_Material();
//                $file_model->addMaterial($file_info);
                
                
                $dirname = $file_info['TypeId'];
                $courseName = $CourseId;

                $namefile = $file_info['name'];
//                if (!file_exists("/var/www/html/osalex/public/".$couesedata[0]['name']."/$dirname")) {
//                    mkdir("/var/www/html/osalex/public/".$couesedata[0]['name']."/$dirname", 0777, true);
//                    $my_model = new Application_Model_Material();
////                  
//                    $my_model->addParent($dirname);
//
////                      mkdir("/var/www/html/zendproject/$coursefile/", 0777, true);
//                }
//                $fin = fopen("/var/www/html/osalex/images/$namefile", "r");
//
//                $fout = fopen("/osalex/public/".$couesedata[0]['name']."/$dirname"."/$namefile", "wb");
//                @ stream_filter_append("/osalex/public/".$couesedata[0]['name']."/$dirname"."/$namefile", $fin);

//       

//                stream_copy_to_stream($fin, $fout);
//                fclose($fin);
//                fclose($fout);
//               
//                    $my_model->addParent($dirname);
                $file_model->addMaterial($file_info);
//               
            }
        }

        $this->view->form = $form;
    }

    //**************************************
    //Download 
    public function downloadAction() {
        $id = $this->_request->getParam('id');
//   $id=8;
        $file_model = new Application_Model_Material();
        $array = $file_model->getMaterialById($id);
        
        $filename = $array[0]['name'];
        $coursename=$array[0]['CourseId'];
        $typeparent=$array[0]['TypeId'];
        $noOfDownloads=$array[0]['numberOfDownloads'];  
        $newdownload=$noOfDownloads+1;
        $course=new Application_Model_Course();
        $couesedata=$course->getCourseById($coursename);
      @  $file_model->countMaterialDownload($id, $newdownload);

        $fin = fopen("/var/www/html/osalex/public/".$couesedata[0]['name']."/$filename", "r");

        $fout = fopen("/home/marina/Desktop/$filename", "wb");
        @ stream_filter_append("/var/www/html/osalex/out/$filename", $fin);

        stream_copy_to_stream($fin, $fout);
        fclose($fin);
        fclose($fout);



//        $this->view->form = $form;
    }

    //**************************************
    public function listAction() {
        $Material_model = new Application_Model_Material();
        $this->view->material = $Material_model->listMaterial();
        // $this->view->material = $Material_model->getMaterialByCourseId(2);
    }

    public function listoneAction() {


        $id = $this->_request->getParam("id");
//      var_dump("----------");
//      var_dump($id);
//        if (!empty($id)) {
        $Mateial_model = new Application_Model_Material();
        // $Mateial_model->getMaterialByCourseId($id);
        $this->view->material = $Mateial_model->getMaterialByCourseId($id);
        //     
        //}
    }

    public function showAction() {


        $id = $this->_request->getParam("id");
//      var_dump("----------");
//      var_dump($id);
//        if (!empty($id)) {

        $Mateial_model = new Application_Model_Material();
        // $Mateial_model->getMaterialByCourseId($id);
        //  $this->view->material =  $Mateial_model->getMaterialByCourseId($id);
        $this->view->material = $Mateial_model->getMaterialByShownId($id);


//              //     
        //}
    }

//ListMaterial
    //**************************************
    //To delete specific 
    public function deleteAction() {
        $id = $this->_request->getParam("id");
        if (!empty($id)) {
            $Mateial_model = new Application_Model_Material();
            $Mateial_model->deleteMateial($id);
        }
        $this->redirect("material/list");
    }

    public function viewAction() {

        $id = $this->_request->getParam("id");
//        var_dump($id);
//             $Mateial_model = new Application_Model_Material();
//            $Mateial_model->getMaterialById($id);
//            var_dump($Mateial_model->getMaterialById($id));

        $Material_model = new Application_Model_Material();

        $this->view->material = $Material_model->getMaterialById($id);
        //var_dump($this->view->materiall);
    }

//Material
    //---------EDIT-------------------------


    public function editAction() {
        $id = $this->_request->getParam("id"); //always comes from get not post
        $form = new Application_Form_Material();
        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getParams())) {

                $material_info = $form->getValues();
                $material_info['id'] = $id;
                $Material_model = new Application_Model_Material();
                $Material_model->editMaterial($material_info);
            }
        }
        if (!empty($id)) {

            $mat_model = new Application_Model_Material();
            $mat = $mat_model->getMaterialById($id);

            $form->populate($mat[0]);
        } else {
            $this->redirect("material/list/");
        }
        $this->view->form = $form;
        $this->render('add');
    }

//Edit Material
    //---------------------Update is shown--------
//     public function showAction() {
//        $id = $this->_request->getParam("id");
//        if (!empty($id)) {
//            $Mateial_model = new Application_Model_Material();
//            $Mateial_model->shownMaterial($id);
//        }
//        $this->redirect("material/list");
//    } //Material
//    
}
