<?php

class AdminController extends Zend_Controller_Action
{

    public function init()
    {
       
    }
    
    
    
    public function indexAction()
    {
        $course=new Application_Model_Course();
        $auth = Zend_Auth::getInstance()->getStorage()->read();
       // var_dump( $auth->lastLogIn);
        $material=new Application_Model_Material();
        $user = new Application_Model_User();
        
       if( $auth->type=='admin')
       {
           $allUsers= $user->getUsersData();
           $allCourses = $course->listCourses();
           $Courseresult=$course->getCoursesFromDate($auth->lastLogIn);
           $latestUsers = $user->getUsersFromDate($auth->lastLogIn);
    //       var_dump($Courseresult);
            for($i=0;$i<count($Courseresult);$i++)
            {            
                $Courseresult[$i]['material']=$material->getMaterialByCourseId($Courseresult[$i]['id']);

                $Courseresult[$i]['TeachedBy'] = $user->getUserVIInfoById($Courseresult[$i]['TeachedBy']);
            }

            $this->view->latestcourses=$Courseresult;
           // var_dump($Courseresult);
            $this->view->latestuser=$latestUsers;
           
            
            $this->view->allusers=$allUsers;
            $this->view->allcourses=$allCourses;           
           
       }
       else 
       {
            $this->redirect("error/error");
       }
    }
    
    public function logoutAction()
    {
        $authM  = Zend_Auth::getInstance();
        $auth  = $authM->getStorage()->read();
        $usr = new Application_Model_Admin();
        $dateObject = new Zend_Date();
        $date = (string)$dateObject->get('YYYY-MM-dd');
        $usr->updateUserLastLogin($date, $auth->id);
        $authM->clearIdentity();
        $this->_redirect('/admin/login');
    }
    
    public function loginAction()
    {
        $form = new Application_Form_Login();
        
        $form->username->setlabel("UserName: ");
        
        $this->view->form = $form;

        if($this->_request->isPost())
        {
            if($form->isValid($this->_request->getParams()))
            {
                $username = $this->_request->getParam("username");
                $password = $this->_request->getParam("password");
                
                
                /*
                 * get connection
                 */
                $db = Zend_Db_Table::getDefaultAdapter();
                
                /*
                 * object from table student has data all data of 2 columns. 
                 */
                $authAdapter = new Zend_Auth_Adapter_DbTable($db,'Admin',"username", 'password');
                
                
                $authAdapter->setIdentity($username);
                $authAdapter->setCredential(md5($password));
                
                
                $result = $authAdapter->authenticate();
                
                
                if ($result->isValid()  )
                {
                    
                    $auth = Zend_Auth::getInstance();
                    $storage = $auth->getStorage();
                    
                    $admin_data=$authAdapter->getResultRowObject(array('id', 'username' ,'img','lastLogIn'));
                    $admin_data->type='admin';
                    $storage->write($admin_data);
                    
                    
                    
                    
                    $this->redirect("admin");
                }
                else
                { 
                    $this->redirect("admin/login");

                }
                
            }
        }
    }
}
?>
