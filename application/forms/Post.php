<?php

class Application_Form_Post extends Zend_Form {

//    protected  $courseId;
//    public function __construct($CourseId) {
//        $this->courseId=$CourseId;
//    }
    public $id;
    public function init() {

        $this->setMethod("post");
        $body = new Zend_Form_Element_Textarea("textArea");
        $body->setLabel("POST: ")
             ->setAttrib("rows", "4")
             ->setAttrib("cols","50");


        $body->setRequired()
             ->setAttrib("rows", "4")
             ->setAttrib("cols","50");
        
       $this->id = new Zend_Form_Element_Hidden("CourseId");
        //$id->setValue( $this->courseId);

        $submit = new Zend_Form_Element_Submit("addpost");
        $this->addElements(array($body, $this->id,$submit));
    }

//Endof /Intit
}
