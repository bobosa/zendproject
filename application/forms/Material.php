<?php

class Application_Form_Material extends Zend_Form {

    public function init() {

//        $name = new Zend_Form_Element_Text("name");
//        $name->setLabel("Material Name: ")
//                ->setRequired();
//(`name`, `isLocked`, `isShown`, `numberOfDownloads`, `parent`, `TypeId`, `CourseId`)
        $isLocked = new Zend_Form_Element_Select("isLocked");
        $isLocked->setLabel("Locked")
                ->setRequired();
        $isLocked->addMultiOptions(array('1' => 'locked', '0' => 'unlocked'));

        $isShown = new Zend_Form_Element_Checkbox("isShown");
        $isShown->setLabel("Shown")
                ->setRequired();
//      $isShown ->multiOptions(array('shown' => '1', 'hide' => '0'));

        $type_model = new Application_Model_Type();
        /* @var $mtype type */
        $mtype = $type_model->listType();
//            
//        var_dump($mtype);
        $size = count($mtype);
        $myarray = array();
        for ($i = 0; $i < $size; $i++) {

            $myarray[$mtype[$i]['id']] = $mtype[$i]['name'];
        }


        $TypeId = new Zend_Form_Element_Select("TypeId");
        $TypeId->setLabel("Type")
                ->setRequired();
        $TypeId->addMultiOptions($myarray);

        $CourseId = new Zend_Form_Element_Hidden("CourseId"); //WillCome From Session
        $CourseId->setValue(1);
              $coursefile=$CourseId->getValue(); 
              $typefile=$TypeId->getValue();
//        echo"--------------";
//              var_dump($typefile);
//        echo '<br><br><br><br><br><br>';
//       var_dump("/var/www/html/zendproject/$coursefile/$typefile");
//           
//           exit();
//            if (!file_exists("/var/www/html/zendproject/$coursefile/$typefile")) {
//               if (!file_exists("/var/www/html/zendproject/$typefile")) {
//                mkdir("/var/www/html/zendproject/$typefile/", 0777, true);
////                      mkdir("/var/www/html/zendproject/$coursefile/", 0777, true);
//            }
//              mkdir("/var/www/html/zendproject/$coursefile/$typefile", 0777, true);
//                       if (!file_exists("/var/www/html/zendproject/go/33")) {
//                       mkdir("/var/www/html/zendproject/go/33", 0777, true);}
//        var_dump(('/var/www/html/zendproject/'.$coursefile.'/'.$typefile.'/'));

//        echo '<br><br><br><br><br><br>';
//        var_dump("/var/www/html/zendproject/$coursefile/$typefile");

//         exit();
//              var_dump("/var/www/html/zendproject/$courseName");
//              }
//                exit();
        $parent = new Zend_Form_Element_File('name');
        $parent->setLabel('Upload Material:')
                //->setDestination('/var/www/html/osalex/public/Images/')
//               ->setDestination('/var/www/html/zendproject/'.$coursefile.'/'.$typefile.'/')
                ->addValidator('NotEmpty')
                ->setMaxFileSize(2097152)
                ->addValidator('Extension', false, 'webm ,jpg, ppt,pdf,png');

//
        $CourseId = new Zend_Form_Element_Hidden("CourseId"); //WillCome From Session
        $CourseId->setValue(1);
//               $this->getElement('your-name')->setValue(1);
//      


        $submit = new Zend_Form_Element_Submit("submit");

        $this->addElements(
                array(
//                    $name,
                    $isLocked,
                    $isShown,
                    $TypeId,
                    $parent,
                    $CourseId,
                    $submit
        ));
    }

}
