<?php

class Application_Form_Login extends Zend_Form
{

    public function init()
    {
        $this->setMethod("post");
        
        $userName =  new Zend_Form_Element_Text("username");
        $password = new Zend_Form_Element_Password("password");
        
        
        $userName
                ->setLabel("UserName or Email or Mobile: ")
                ->setRequired()
                ->addFilter(new Zend_Filter_StripTags);
        
        $password->setRequired()
                 ->setLabel("Password");
        
        $submit = new Zend_Form_Element_Submit("submit");
        
        $this->addElements(
                array($userName,
                    $password,
                    $submit
                ));
    }


}

