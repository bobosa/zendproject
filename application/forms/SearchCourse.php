<?php

class Application_Form_SearchCourse extends Zend_Form
{

    public function init()
    {
        $this->setMethod("post");
        
        $query =  new Zend_Form_Element_Text("query");
        $query->setLabel("Course Name: ")
             ->setRequired()
             ->addFilter(new Zend_Filter_StripTags);
        $tag=new Zend_Form_Element_Multiselect('Tags');
        $tags=new Application_Model_Tags();
        $all=$tags->listTags();
        $TagsToAdd=array();
        for($i=0;$i<count($all);$i+=1)
        {
               $TagsToAdd[$all[$i]['id']]=$all[$i]['name'];
        }
        $tag->addMultiOptions($TagsToAdd);
        $submit=new Zend_Form_Element_Submit("search");
        
        $submit->setAttrib("class","btn btn-success");
        $this->addElements(array($query, $submit));
    }


}