<?php

class Application_Form_Comment extends Zend_Form
{

     public $id;

    public function init()
    {
        $this->setMethod("post");
        $body = new Zend_Form_Element_Textarea("textArea");
        $body->setLabel("Comment: ");
        $body->setRequired()
             ->setAttrib("rows", "4")
             ->setAttrib("cols","50");

        $submit = new Zend_Form_Element_Submit("submit");

        
        $this->addElements(array($body,$submit));   
        }



}

