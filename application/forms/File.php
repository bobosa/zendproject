<?php

class Application_Form_File extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */

        
        $element = new Zend_Form_Element_File('name');
        $element->setLabel('Upload an image:')
                ->setDestination('/var/www/html/zendproject/images/')
                ->addValidator('NotEmpty')
                ->setMaxFileSize(2097152)
                ->addValidator('Extension', false, 'mov, avi, wmv , jpg, ppt,pdf,png, mp4');
        $submit = new Zend_Form_Element_Submit("submit");
        $submit->setAttrib('class', 'btn btn-primary');
        $this->addElements(array($element, $submit));
        
    }

}




