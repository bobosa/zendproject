<?php

class Application_Form_Course extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
         $this->setMethod("post");
        $name=new Zend_Form_Element_Text("name");        
        $name->setLabel("Course Name: ")
             ->setRequired()
             ->addFilter(new Zend_Filter_StripTags);
        $describtion=new Zend_Form_Element_Textarea("describtion");
        $describtion->setLabel("Course description: ")
             ->setRequired()

             ->addFilter(new Zend_Filter_StripTags)
             ->setAttrib("rows", "4")
             ->setAttrib("cols","50");
//        
//        $path=new Zend_Form_Element_File("path");
//        $path->setLabel("Course Path: ")
//             ->setRequired()
//             ->addFilter(new Zend_Filter_StripTags);
        
        $status=new Zend_Form_Element_Select("status");
        $status->setLabel("Course Status: ")
             ->setRequired()
             ->addFilter(new Zend_Filter_StripTags);
//        $option=new Zend_Form_Element_Select

        $status->addMultiOptions(array(
        "finished" => "finished",
        "still_working" => "still working",
    ));
        $Tags=new Zend_Form_Element_Multiselect("Tags");
        $TagsToAdd=new Application_Model_Tags();
        $optionTochoose=$TagsToAdd->listTags();
        $option=array();
        for($i =0 ; $i<count($optionTochoose) ; $i++)
        {
         $option[$optionTochoose[$i]['id']]=  $optionTochoose[$i]['name']; 
        }
       $Tags->addMultiOptions($option);
        
        
        $image = new Zend_Form_Element_File("Img");
        $image
                ->setLabel("Image: ")
                ->setRequired()
                ->setDestination('/var/www/html/osalex/public/Images/courseImg/')
                ->addValidator(new Zend_Validate_File_IsImage());
        $submit=new Zend_Form_Element_Submit("Add");
        $submit->setAttrib("class","btn btn-success");
        $this->addElements(array($name,$describtion,$status,$Tags,$image, $submit));
    }


}

