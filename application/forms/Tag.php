<?php

class Application_Form_Tag extends Zend_Form
{

    public function init()
    {
       
        $this->setMethod("post");
        $name =  new Zend_Form_Element_Text("name");
        $description = new Zend_Form_Element_Textarea("describtion");
        $name
                ->setLabel("Tag Name: ")
                ->setRequired()
                ->addFilter(new Zend_Filter_StripTags);
        $description
                ->setLabel("Tag Description: ")
                ->setRequired()
                ->addFilter(new Zend_Filter_StripTags);
         
        $submit = new Zend_Form_Element_Submit("submit");
        
        $this->addElements(array( $name,$description,$submit));
    }


}

