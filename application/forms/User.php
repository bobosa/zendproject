<?php


class Application_Form_User extends Zend_Form
{

    public function init()
    {
        $this->setMethod("post");
        
        $userName =  new Zend_Form_Element_Text("username");
        $firstName = new Zend_Form_Element_Text("firstname");
        $lastName = new Zend_Form_Element_Text("lastname");
        $middleName = new Zend_Form_Element_Text("middlename");
        $email = new Zend_Form_Element_Text("email");     
        $mobile = new Zend_Form_Element_Text("mobile");
        $country = new Zend_Form_Element_Select("country");
        $image = new Zend_Form_Element_File("image");
        $password = new Zend_Form_Element_Password("password");
        $confirmPassword = new Zend_Form_Element_Password("confirmpassword");
        $gender = new Zend_Form_Element_Select("gender");
        $birthday = new Zend_Form_Element_Text("birthday");
        $preferedTags = new Zend_Form_Element_Multiselect("preferedtags");
//        $captcha = new Zend_Form_Element_Captcha("captcha");
        
        $userName
                ->setLabel("User Name: ")
                ->setRequired()
                ->addFilter(new Zend_Filter_StripTags)
                ->addValidator(new Zend_Validate_Db_NoRecordExists(
                    array(
                        "table"=>"User",
                        "field"=>"userName"
                    )
                    ));
        
        
        $firstName
                ->setLabel("First Name: ")
                ->setRequired();
        
        
        $middleName
                ->setLabel("Middle Name: ");
        
        
        $lastName
                ->setLabel("Last Name: ")
                ->setRequired();
    
        $gender
                ->setRequired()
                ->setLabel("Gender: ")
                ->addMultiOptions(array('Male'=>'Male','Female'=>'Female'));
        
        $email
            ->setRequired()
            ->setLabel("Email: ")
            ->addValidator(new Zend_Validate_EmailAddress())
            ->addValidator(new Zend_Validate_Db_NoRecordExists(
                    array(
                        "table"=>"User",
                        "field"=>"email"
                    )
                    ));
        
        $birthday
                ->setLabel("Birthday")
                ->setAttrib("class", "datepicker")
                ->setRequired();
        $password
                ->setRequired()
                ->setLabel("Password: ")
                ->addValidator('StringLength', false, array(6,24))
                ->addFilter(new Zend_Filter_StringTrim());
        
        $confirmPassword
                ->setRequired()
                ->setLabel("Confirm Password: ")
                ->addFilter(new Zend_Filter_StringTrim())
                ->addValidator(new Zend_Validate_Identical('password'));
            
        
        $country
                ->setLabel("Country: ")
                ->setRequired()
                ->setAttrib("class", "chosen-container chosen-container-single")
                ->addMultiOptions(
                     Zend_Locale::getTranslationList('Territory','en',2), Zend_Locale::getTranslationList('Territory','en',2)
                 );
        
        
        $TagsToAdd=new Application_Model_Tags();
        $optionTochoose=$TagsToAdd->listTags();
        $option=array();
        for($i =0 ; $i<count($optionTochoose) ; $i++)
        {
         $option[$optionTochoose[$i]['id']]=  $optionTochoose[$i]['name']; 
        }
        
        $preferedTags
                ->setRequired()
                ->setLabel("Prefered Tags: ")
                ->addMultiOptions($option)
                ->setAttrib("class", "default");
        
//        $captcha = new Zend_Captcha_Image();
//        
//        $captcha->setWordLen("4")
//        ->setHeight("60")
//        ->setFont(new Zend_Pdf_Resource_Font_Simple_Standard_TimesRoman())
//        ->setImgDir('http://project.com/captcha/')
//        ->setDotNoiseLevel("5") 
//        ->setLineNoiseLevel("5");
//
//        
//                
//                        
//            $captcha->generate(); 
        
        $image
                ->setLabel("Image: ")
                ->setRequired()
                ->setDestination('/var/www/html/osalex/public/Images/')
                ->addValidator(new Zend_Validate_File_IsImage());
        
                
        
        
        $mobile
                ->setLabel("Mobile: ")
                ->setRequired();
        
        
        $submit = new Zend_Form_Element_Submit("submit");
        
//        $this->addElement('captcha', 'captcha', array(
//            'label' => "Show that U are n't a machine:",
//            'required' => true,
//            'captcha' => array(
//                'captcha' => 'figlet',
//                'wordLen' => 5,
//                'timeout' => 300,
//                //'imgUrl' => 'http://project.com/captcha/', 
//            )
//        ));
        $this->addElements(
                array(
                    $firstName,
                    $middleName,$lastName,
                    $gender,$country,
                    $birthday , 
                    $userName,
                    $email,$mobile ,
                    $password,$confirmPassword,
                    $image , $preferedTags ,
                    $submit
                ));
        
        
        
        
        
    }


}

