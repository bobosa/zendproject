<?php

class Application_Model_Admin extends Zend_Db_Table_Abstract
{
    protected $_name = "Admin";
    
    function addUser($data)
    {
        $row = $this->createRow();
        
        $row->userName = $data['username'];
        $row->firstName = $data['firstname'];
        
        $row->middleName = $data['middlename'];
        $row->lastName = $data['lastname'];
        $row->mobile = $data['mobile'];
        $row->country = $data['country'];
        $date= new Zend_Date($data['birthday'],"MM/dd/YYYY");
        $row->birthday = (string)$date->get('YYYY-MM-dd');
        $row->password = md5($data['password']);
        $row->email = $data['email'] ;
        $row->gender = $data['gender'];
        $row->image = $data['image'];
//        $date=new Zend_Date();
//        $row->becomeMember=(string)$data->get('YYYY-MM-dd HH:mm:ss');
        
        
        
        
        $row->save();
        
        
        $insertId = $this->getAdapter()->lastInsertId();
        
        return $insertId;
        
        
    }
    
    function listUsers()
    {   
        return $this->fetchAll()->toArray();
    }
    
    function getUserById($id)
    {
        return $this->find($id)->toArray();
    }
    
            
    function editUser($data)
    {
        if(!empty($data['password']))
            $data['password']=md5($data['password']);
        else
            unset ($data['password']);
        $this->update($data, "id=".$data['id']);
        return $this->fetchAll()->toArray();
    }
    


    

    function deleteUser($id){
        return $this->delete("id=$id");
    }
    
    function changePassword($id,$password)
    {
        return $this->update(array("password"=>"md5($password)"),'id = ?', $id );
    }

   function getUserVIInfoById($id)
    {
         $result =  $this->select()->from($this,array('id','firstName','image'))
                                    ->where("id =$id");
         return $this->fetchAll($result)->toArray();
           
    }
    
    function updateUser($data,$id)
    {
       // var_dump($data);
        $result = $this->update($data, "id=".$id);

        return $result;
    }
    
    function updateUserLastLogin($data,$id)
    {
        $da = array('lastLogIn' => $data);
        $result = $this->update($da, "id=".$id);

        return $result;
           
    }
}
