<?php

class Application_Model_Course extends Zend_Db_Table_Abstract
{
    
    protected $_name = "Course";
    
      public function addCourse($data){
       
        $row = $this->createRow();
        $row->name = $data['name'];
        $row->describtion = $data['describtion'] ;
        $row->path=$data['path'];
        $row->TeachedBy=$data['TeachedBy'];
        $date=new Zend_Date();
       // $date =new Zend_date("Y-m-d H:i:s"); 
        $row->publishDate=(string)$date->get('YYYY-MM-dd HH:mm:ss');
        $row->status=$data['status'];
        $row->Img=$data['Img'];
        return $row->save();
   
        }
        public function getLastCourse()
        {
            $sql = 'SELECT max(id) FROM Course';  
            $query = $this->getAdapter()->query($sql);
            $result = $query->fetchAll();
            return $result[0]['max(id)']; 
        }
        public function listCourses()
        {
        
        return $this->fetchAll()->toArray();
        }
        public function deleteCourse($id) 
        {
             return $this->delete("id=$id");
        }
        public function getCourseById($id){
            return $this->find($id)->toArray();
        }
        public function editCourse($data,$tags){

        $this->update($data, "id=".$data['id']);
        $CourseHasTag=new Application_Model_CourseHasTag();
        $CourseHasTag->deleteCourseTags($data['id']);
        $CourseHasTag->addCourseTags($data['id'],$tags);          
   
        }
        
        public function searchCourse($tagsid,$namelike)
        {
            $sql = 'SELECT * FROM Course '
                    . "where name like '%".$namelike."%' ";  
            $query = $this->getAdapter()->query($sql);
            $result = $query->fetchAll();
            return $result; 
        }

        
        //Khokha
//         public function getInstractorIdbyCourse($id)
//        {
//            $sql = "SELECT  TeachedBy FROM Course where id=".$id;  
//            $query = $this->getAdapter()->query($sql);
//            return $query->fetchAll();
//        }

        public function getCoursesFromDate($date)
        {
            $sql = 'SELECT * FROM Course '
                    . "where publishDate > ".$date;  
            $query = $this->getAdapter()->query($sql);
            $result = $query->fetchAll();
            return $result; 
            
        }
        
        public function getUserCourses($id)
        {
            $sql = 'SELECT * FROM Course '
                    . "where TeachedBy = ".$id;
            
            $query = $this->getAdapter()->query($sql);
            $result = $query->fetchAll();
            return $result;
        }
        
        

}

