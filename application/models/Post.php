<?php

class Application_Model_Post extends Zend_Db_Table_Abstract
{


    protected $_name="Post";
//------------------------------------------------------------------------   
//

    //To Add New Post

      function addPost($data) {
        //var_dump($data);exit;
        $this->_integrityChecks = FALSE;
        $row = $this->createRow();
        $row->textArea = $data['textArea'];
        $row->date = $data['date'];
        $row->UserId = $data['UserId'];
        $row->CourseId = $data['CourseId'];
        return $row ->save();

}
    //----------------------------------------------------
    //To list all Post 

    function listPost(){
        
        return $this->fetchAll()->toArray();
    }
    //--------------------------------------------------------------
    
    //To delete post
    
    function deletePost($id)
    {
        return $this->delete("id=$id");
        
    }
    //------------------------------------------------------------------
    
    
    function getPostById($id){
        return $this->find($id)->toArray();
    }
     //-------------------------------------------------------  
    //to edit Post
    function editPost($data){       
        return  $this->update($data, "id=".$data['id']);
//        return $this->fetchAll()->toArray();
    }
  //-------------------------------------------------------------
//    function showPost($id) {
//       // return $this->select()->columns('')->where($cond);
//        
//       return $this->getPostById($id);
//                     
//       
//    }
    
    //


      public function getPostsByCourseId($id)
      {
            $sql = "SELECT * FROM Post where CourseId=".$id." order by date DESC";  
            $query = $this->getAdapter()->query($sql);
            return $query->fetchAll();
           
      }
        
      
}




