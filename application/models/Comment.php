<?php


class Application_Model_Comment extends Zend_Db_Table_Abstract

{
    
//     show columns from Comment;
//+----------+----------+------+-----+---------+----------------+
//| Field    | Type     | Null | Key | Default | Extra          |
//+----------+----------+------+-----+---------+----------------+
//| id       | int(11)  | NO   | PRI | NULL    | auto_increment |
//| textArea | text     | NO   |     | NULL    |                |
//| date     | datetime | NO   |     | NULL    |                |
//| UserId   | int(11)  | NO   | MUL | NULL    |                |
//| PostId   | int(11)  | NO   | MUL | NULL    |                |
//+----------+----------+------+-----+---------+----------------+

protected $_name = "Comment";  //table name in database

function addComment($data)
{
        $row = $this->createRow();
        $row->textArea = $data['textArea'];
        $date=new Zend_Date();            
        $row->date =(string)$date->get('YYYY-MM-dd HH:mm:ss');
        $row->UserId = $data['UserId'];
        $row->PostId = $data['PostId'];
        return $row ->save();
}
    

//GetComment according to Post Id
function getComments($postId){
        
        $result = $this->select()
                ->where(" PostId =$postId");
     return $this->fetchAll($result)->toArray();
        //return $res;
    }
    
    
   
function deleteComment($data)
{
    
    return $this->delete("id=$data");
}
    
 function getCommentId($id) {
        return $this->find($id)->toArray();
    }
  
  function editComment($data)
    {
//       
            $this->update($data, "id=".$data['id']);
        return $this->fetchAll()->toArray();
    }
    


}

