<?php


class Application_Model_Tags  extends Zend_Db_Table_Abstract
{

    protected $_name = "Tag";
    
    public function addTag($data){
       
        $row = $this->createRow();
        $row->name = $data['name'];
        $row->describtion = $data['describtion'] ;
        return $row->save();
   
        }
    public function listTags(){
        
        return $this->fetchAll()->toArray();
    }
    public function getTagById($id){
        return $this->find($id)->toArray();
//        var_dump($this->find($id)->toArray());
//        exit();
    }
      
    public function editTag($data){

        return  $this->update($data, "id=".$data['id']);
   
        } 
    public function deleteTag($id){
        return $this->delete("id=$id");
    }
    
    public function searchTag($namelike)
    {
        $sql = 'SELECT id FROM Tag '
                . "where name like '%".$namelike."%' ";
        
        $query = $this->getAdapter()->query($sql);
        $result = $query->fetchAll();
        return $result; 
    }

}

